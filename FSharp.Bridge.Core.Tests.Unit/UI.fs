﻿module TypeTests

open Xunit
open Swensen.Unquote

open FSharp.Bridge.Core
open FSharp.Bridge.Core.Types
open FSharp.Bridge.Core.UI

[<Fact>]
let ``Four of hearts correct toString`` () =
    let card = Hearts *| Four

    test <@ card |> printCard = "♥4" @>