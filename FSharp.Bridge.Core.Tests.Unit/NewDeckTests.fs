﻿module FSharp.Bridge.Core.Tests.Unit.NewDeckTests

open Xunit
open Swensen.Unquote

open FSharp.Bridge.Core
open FSharp.Bridge.Core.Setup
open FSharp.Bridge.Core.Types

[<Fact>]
let ``New deck returns 52 cards`` () = 
    let deck = newDeck()

    test <@ deck |> Seq.length = 52 @>

[<Fact>]
let ``New deck returns all unique cards`` () = 
    let deck = newDeck()

    test <@ deck |> Seq.distinct |> Seq.length = (deck |> Seq.length) @>