﻿module FSharp.Bridge.Core.Tests.Unit.DealTests

open Xunit
open Swensen.Unquote

open FSharp.Bridge.Core
open FSharp.Bridge.Core.Setup

open FSharp.Bridge.Core.Types

[<Fact>]
let ``Four cards are dealt correctly`` () =
    let deck = [Hearts *| Four; Clubs *| Ten; Clubs *| Jack; Spades *| Ace]

    let hand1, hand2, hand3, hand4 = deal deck

    test <@ hand1 = [Hearts *| Four] @>
    test <@ hand2 = [Clubs *| Ten] @>
    test <@ hand3 = [Clubs *| Jack] @>
    test <@ hand4 = [Spades *| Ace] @>

[<Fact>]
let ``Eight cards are dealt correctly`` () =
    let deck = 
        [Hearts *| Four
         Hearts *| Seven
         Clubs *| Two
         Clubs *| Ace
         Clubs *| Jack
         Clubs *| Five
         Spades *| Eight
         Spades *| Five]

    let hand1, hand2, hand3, hand4 = deal deck

    test <@ hand1 = [Hearts *| Four; Clubs *| Jack] @>
    test <@ hand2 = [Hearts *| Seven; Clubs *| Five] @>
    test <@ hand3 = [Clubs *| Two; Spades *| Eight] @>
    test <@ hand4 = [Clubs *| Ace; Spades *| Five] @>