﻿module FSharp.Bridge.Core.Tests.Unit.PlayTests

open Xunit
open Swensen.Unquote

open FSharp.Bridge.Core
open FSharp.Bridge.Core.Types
open FSharp.Bridge.Core.Play

[<Fact>]
let ``Lay card test`` () =
    let card = Clubs *| Seven
    let player = { position = North; name = "Player 1" }
    let state = Waiting

    let actual = layCard card state player

    test <@ actual = OneCardDown ((Clubs *| Seven), player) @>


[<Fact>]
let ``getPlayable cards with no trumps and one lead suit card`` () =
    let hand =
        [Hearts *| Two; Hearts *| Queen; Clubs *| Nine]

    let suitLead = Clubs

    let actual = getPlayableCards hand suitLead

    test <@ actual = [Clubs *| Nine] @>


[<Fact>]
let ``getPlayable cards with no trump but lead suit cards`` () =
    let hand =
        [Hearts *| Two; Hearts *| Queen; Clubs *| Nine; Spades *| Ten]

    let suitLead = Clubs

    let actual = getPlayableCards hand suitLead

    test <@ actual = [Clubs *| Nine] @>


[<Fact>]
let ``getPlayable cards with no trump and no lead suit cards`` () =
    let hand =
        [Hearts *| Two; Hearts *| Queen; Diamonds *| Nine; Spades *| Ten]

    let suitLead = Clubs

    let actual = getPlayableCards hand suitLead

    test <@ actual = [Hearts *| Two; Hearts *| Queen; Diamonds *| Nine; Spades *| Ten] @>



[<Fact>]
let ``getWinner correctly determines winner with no trumps`` () =
    let mutable roundState = Waiting
    let contract : Bidding.Contract =
        { trumpSuit = Bidding.NoTrumps; level = Bidding.One }

    let north, east, south, west =
        { position = North; name = "North" },
        { position = East; name = "East" },
        { position = South; name = "South" },
        { position = West; name = "West" }

    roundState <-  layCard (Hearts *| Two) roundState north
    roundState <-  layCard (Hearts *| Four) roundState east
    roundState <-  layCard (Clubs *| Ace) roundState south
    roundState <-  layCard (Hearts *| Ten) roundState west

    let actual = getWinner roundState contract

    test <@ actual = west @>

[<Fact>]
let ``getWinner correctly determines winner with Spades as trumps`` () =
    let mutable roundState = Waiting
    let contract : Bidding.Contract =
        { trumpSuit = Bidding.Trumps Spades; level = Bidding.One }

    let north, east, south, west =
        { position = North; name = "North" },
        { position = East; name = "East" },
        { position = South; name = "South" },
        { position = West; name = "West" }

    roundState <-  layCard (Clubs *| King) roundState north
    roundState <-  layCard (Clubs *| Ace) roundState east
    roundState <-  layCard (Spades *| Ten) roundState south
    roundState <-  layCard (Clubs *| Two) roundState west

    let actual = getWinner roundState contract

    test <@ actual = south @>