﻿module FSharp.Bridge.Core.Tests.Unit.PointsTests

open Xunit
open Swensen.Unquote

open FSharp.Bridge.Core
open FSharp.Bridge.Core.Types
open FSharp.Bridge.Core.Points

[<Fact>]
let ``Example 1) adds up correctly`` () =
    let hand =
        [ Spades *| Ace ]

    test <@ countHighCardPoints hand = 4 @>

[<Fact>]
let ``Example 2) adds up correctly`` () =
    let hand =
        [ Hearts *| Ace
          Spades *| Queen
          Spades *| Four
          Spades *| King
          Hearts *| Three
          Diamonds *| Two
          Clubs *| Jack ]

    test <@ countHighCardPoints hand = 10 @>

[<Fact>]
let ``Adds length points correctly with one long suit`` () =
    let hand =
        [ Hearts *| Ace
          Spades *| Queen
          Spades *| Four
          Spades *| King
          Spades *| Ace
          Spades *| Eight
          Hearts *| Three
          Diamonds *| Two
          Clubs *| Jack ]

    test <@ countLengthPoints hand = 1 @>

[<Fact>]
let ``Adds length points correctly with two long suits`` () =
    let hand =
        [ Hearts *| Ace
          Hearts *| Two
          Hearts *| Jack
          Hearts *| King
          Hearts *| Three
          Spades *| Queen
          Spades *| Four
          Spades *| King
          Spades *| Ace
          Hearts *| Four
          Hearts *| Eight
          Spades *| Eight
          Diamonds *| Two
          Clubs *| Jack ]

    test <@ countLengthPoints hand = 4 @>

[<Fact>]
let ``Adds total points correctly`` () =
    let hand =
        [ Hearts *| Ace
          Hearts *| Two
          Hearts *| Jack
          Hearts *| King
          Hearts *| Three
          Spades *| Queen
          Spades *| Four
          Spades *| King
          Spades *| Ace
          Hearts *| Four
          Hearts *| Eight
          Spades *| Eight
          Diamonds *| Two
          Clubs *| Jack ]

    test <@ countTotalPoints hand = 22 @>