﻿module FSharp.Bridge.Core.Tests.Unit.ShuffleTests

open Xunit
open Swensen.Unquote

open FSharp.Bridge.Core
open FSharp.Bridge.Core.Setup
open FSharp.Bridge.Core.Types

[<Fact>] 
let ``Deck with one card is returned`` () =
    let deck = [Hearts *| King]

    let shuffledDeck = shuffle deck

    test <@ deck = shuffledDeck @>