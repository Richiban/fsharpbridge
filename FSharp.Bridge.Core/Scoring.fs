﻿module FSharp.Bridge.Core.Scoring

open FSharp.Bridge.Core.Types
open FSharp.Bridge.Core.Bidding

let intValue = 
    function
    | One -> 1
    | Two -> 2
    | Three -> 3
    | Four -> 4
    | Five -> 5
    | Six -> 6
    | Seven -> 7

let (|NoTrumps|MinorSuit|MajorSuit|) =
    function
    | NoTrumps -> NoTrumps
    | Trumps Spades | Trumps Hearts -> MajorSuit
    | Trumps Diamonds | Trumps Clubs -> MinorSuit

let getContractScore (contract : Contract) =
    match contract.trumpSuit with
    | NoTrumps -> (intValue contract.level) * 30 + 10
    | MajorSuit -> (intValue contract.level) * 30
    | MinorSuit -> (intValue contract.level) * 20