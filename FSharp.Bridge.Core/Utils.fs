﻿//module FSharp.Bridge.Core.Utils

namespace Microsoft.FSharp.Collections

module Seq =

    let walkGroups groups =
        groups |> Seq.map (fun (key, items) -> key, Seq.toArray items)

    let having f =
        Seq.filter (fun (key, items) -> f items)
