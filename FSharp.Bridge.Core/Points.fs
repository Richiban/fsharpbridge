﻿module FSharp.Bridge.Core.Points

open Microsoft.FSharp.Collections
open FSharp.Bridge.Core.Types

let highCardPoints card =
    match card.faceValue with
    | Ace -> 4
    | King -> 3
    | Queen -> 2
    | Jack -> 1
    | _ -> 0

let countHighCardPoints hand =
    hand |> Seq.sumBy highCardPoints

let countLengthPoints hand =
    let numCardsForLongSuit = 4

    hand 
    |> Seq.groupBy suit
    |> Seq.choose ( fun (suit, cards) ->
        let numCardsInSuit = Seq.length cards

        if numCardsInSuit > numCardsForLongSuit 
        then Some (numCardsInSuit - numCardsForLongSuit) 
        else None )
    |> Seq.sum

let countTotalPoints hand =
    countHighCardPoints hand + countLengthPoints hand