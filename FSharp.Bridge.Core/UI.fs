﻿module FSharp.Bridge.Core.UI

open FSharp.Bridge.Core.Types

let printSuit =
    function
    | Hearts -> "♥"
    | Clubs -> "♣"
    | Diamonds -> "♦"
    | Spades -> "♠"

let printFaceValue =
    function
    | Two -> "2"
    | Three -> "3"
    | Four -> "4"
    | Five -> "5"
    | Six -> "6"
    | Seven -> "7"
    | Eight -> "8"
    | Nine -> "9"
    | Ten -> "T"
    | Jack -> "J"
    | Queen -> "Q"
    | King -> "K"
    | Ace -> "A"

let printCard card = sprintf "%s%s" (printSuit card.suit) (printFaceValue card.faceValue)

let printCards cs = List.map printCard cs