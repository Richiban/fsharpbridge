﻿module FSharp.Bridge.Core.Play

open FSharp.Bridge.Core.Bidding
open FSharp.Bridge.Core.Types

type LaidCard = Card * Player

type RoundState =
    | Waiting
    | OneCardDown of LaidCard
    | TwoCardsDown of LaidCard * LaidCard
    | ThreeCardsDown of LaidCard * LaidCard * LaidCard
    | FourCardsDown of LaidCard * LaidCard * LaidCard * LaidCard

let layCard card state player =
    match state with
    | Waiting -> OneCardDown (card, player)
    | OneCardDown c -> TwoCardsDown (c, (card, player))
    | TwoCardsDown (c1, c2) -> ThreeCardsDown (c1, c2, (card, player))
    | ThreeCardsDown (c1, c2, c3) -> FourCardsDown (c1, c2, c3, (card, player))
    | FourCardsDown _ -> failwith "Cannot lay another card! Table is full"

let getPlayableCards hand suitLead =
    let suitGroups =
        hand
        |> Seq.groupBy suit
        
    match suitGroups |> Seq.tryFind (fst >> (=) suitLead) with
    | Some (_, followSuitCards) -> followSuitCards |> Seq.toList
    | None -> hand

let getWinner roundState contract =
    match roundState with
    | FourCardsDown (c1, c2, c3, c4) ->
        let suitLead = c1 |> fst |> suit

        let suitGroups =
            [c1; c2; c3; c4]
            |> Seq.groupBy (fst >> suit)
            |> dict

        match contract.trumpSuit with
        | Trumps trumpSuit ->
            suitGroups.[trumpSuit] |> Seq.maxBy (fst >> faceValue) |> snd
        | _ ->
            suitGroups.[suitLead] |> Seq.maxBy (fst >> faceValue) |> snd


    | _ -> failwith "Cannot determine winner when all the cards are not down"