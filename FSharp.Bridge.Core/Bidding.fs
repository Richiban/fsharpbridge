﻿module FSharp.Bridge.Core.Bidding

open FSharp.Bridge.Core.Types

type ContractLevel =
    One | Two | Three | Four | Five | Six | Seven

type TrumpSuitOption =
    NoTrumps | Trumps of Suit

type Contract =
    { trumpSuit : TrumpSuitOption; level : ContractLevel }