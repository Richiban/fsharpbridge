﻿module FSharp.Bridge.Core.Setup

open FSharp.Bridge.Core.Types

open System
open System.Collections.Generic

let (+=) r i = r := !r + i

let deal shuffledDeck : Hand * Hand * Hand * Hand =
    let mutable hand1, hand2, hand3, hand4 = [], [], [], []

    let i = ref 0
    for card in shuffledDeck do
        let hand =
            match !i % 4 with
            | 0 -> hand1 <- (card :: hand1 |> List.sort)
            | 1 -> hand2 <- (card :: hand2 |> List.sort)
            | 2 -> hand3 <- (card :: hand3 |> List.sort)
            | _ -> hand4 <- (card :: hand4 |> List.sort)

        i += 1

    hand1, hand2, hand3, hand4

let shuffle deck : Deck =
    deck |> List.sortBy (fun _ -> Guid.NewGuid())

let allValues = 
    [Two; Three; Four; Five; Six; Seven; Eight; Nine; Ten; Jack; Queen; King; Ace]

let allSuits = 
    [Hearts; Clubs; Diamonds; Spades]

let newDeck () =
    [ for suit in allSuits do
        for value in allValues do
            yield suit *| value ]

let startGame = newDeck >> shuffle >> deal