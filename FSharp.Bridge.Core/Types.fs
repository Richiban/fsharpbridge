﻿module FSharp.Bridge.Core.Types

type Suit = Hearts | Clubs | Diamonds | Spades

type FaceValue = 
  Two | Three | Four | Five | Six | Seven | Eight | Nine | Ten | Jack | Queen | King | Ace

type Card = { suit : Suit; faceValue : FaceValue }
        
let ( *| ) suit face = { suit = suit; faceValue = face }

let suit card = card.suit
let faceValue card = card.faceValue

type Deck = Card list

type Hand = Card list

type PlayerPosition = North | South | East | West

type Player = { position: PlayerPosition; name : string }

type Trick = {
    northPlayed : Card
    eastPlayed : Card
    southPlayed : Card
    westPlayed : Card
}